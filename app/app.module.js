var app = angular.module("uxInsured", ["ngRoute"]);

app.controller("insured", ["$scope", "$location", "$timeout", "uxConfig", "user", function ($scope, $location, $timeout, uxConfig, user) {
    //Initialize the user object
    $scope.insuredData = {
        user: {},
        policies: []
    };
    $scope.insuredData.user = {};
    $scope.insuredData.policies = [];
    $scope.alerts = [];
    hello.init(uxConfig.clientIds);
    hello.on("auth.login", function (auth) {
        // Call user information, for the given network
        hello(auth.network).api('me').then(function (profile) {
            $scope.$apply(function () {
                $scope.alert("Logging in as " + profile.name);
                $scope.insuredData.user.name = profile.name;
                $scope.insuredData.user.id = profile.id;
                $scope.insuredData.user.thumbnail = profile.thumbnail;
                $scope.insuredData.user.loginType = auth.network;
                $scope.insuredData.user.loginDescription = (auth.network.charAt(0).toUpperCase() + auth.network.slice(1));
                $location.path("/associatedPolicies")
            });
        });
    });
    $scope.logout = function () {
        if ($scope.insuredData.user.id) {
            hello($scope.insuredData.user.loginType).logout().then(function (response) {
                $scope.$apply(function () {
                    $scope.insuredData.user = {};
                    $location.path("/")
                });
            });
        } else {
            $location.path("/")
        }
    };
    $scope.navigateHome = function() {
        if ($scope.insuredData.user.id) {
            $location.path("/associatedPolicies")
        } else {
            $location.path("/")
        }
    };
    $scope.alert = function(text, isError) {
        $scope.alerts.push({ text: text, isError: isError });
        $timeout.cancel($scope.alertClear);
        $scope.alertClear = $timeout(function() {
            $scope.alerts = [];
        }, 4000)
    };
    $scope.navigate = function(page, policyIndex) {
        if (policyIndex) $location.search("policyIndex", policyIndex);
        $location.path("/" + page);
    };
    /** Formats a date for user display */
    $scope.formatDate = function(value) {
        return (new Date(value)).toLocaleDateString();
    };
    /** Given a policy object or a paperless code string, returns TRUE or FALSE. */
    $scope.isPaperless = function(paperlessCode) {
        if (!paperlessCode) return null;
        if (typeof paperlessCode === "string") {
            return (paperlessCode === "E");
        } else {
            return (paperlessCode.PAPERLESS === "E");
        }
    }
    /** Retrieves all policies associated with the current user. */
    $scope.refreshPolicies = function() {
        if ($scope.insuredData.user.id) {
            return user.getUser($scope.insuredData.user.id)
                .then(function(associatedPolicies) {
                    $scope.insuredData.policies = associatedPolicies;
                    return associatedPolicies;
                })
                .catch(function(err) {
                    $scope.alert("There was a problem loading your policies.", true);
                })
        }
    };
    /** Returns a google maps link for the agent on a policy. */
    $scope.getAgentMapLink = function(policy) {
        if (!policy.AGENTADDRESS || !policy.AGENTCITY || !policy.AGENTSTATE || !policy.AGENTPOSTAL) return "";
        var address = policy.AGENTADDRESS + ", " + policy.AGENTCITY + ", " + policy.AGENTSTATE + " " + policy.AGENTPOSTAL;
        return "https://maps.google.com/maps?q=" + encodeURIComponent(address);
    };
}]);

//Get configuration asynchronously and then bootstrap the application manually. This ensures
//  we will have the configuration data available.
angular.element(document).ready(
    function () {
        var initInjector = angular.injector(['ng']);
        var $http = initInjector.get('$http');
        $http.get("config/insured-config.json").then(
            function (response) {
                app.constant('uxConfig', response.data);
                angular.bootstrap(document, ["uxInsured"]);
            }
        );
    }
);