app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "app/logins.html",
            controller: "logins"
        })
        .when("/findPolicy", {
            templateUrl: "app/findPolicy.html",
            controller: "findPolicy"
        })
        .when("/associatedPolicies", {
            templateUrl: "app/associatedPolicies.html",
            controller: "associatedPolicies"
        })
        .when("/policySummary", {
            templateUrl: "app/policySummary.html",
            controller: "policySummary"
        })
        .when("/policyBilling", {
            templateUrl: "app/policyBilling.html",
            controller: "policyBilling"
        })
        .when("/policyPaperless", {
            templateUrl: "app/policyPaperless.html",
            controller: "policyPaperless"
        })
        .when("/policyDocuments", {
            templateUrl: "app/policyDocuments.html",
            controller: "policyDocuments"
        })                                        
})