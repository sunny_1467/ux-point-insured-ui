app.controller("associatedPolicies", ["$scope", "$location", "uxConfig", "policy", "user", function($scope, $location, uxConfig, policy, user) {
    $location.search("policyIndex", null);
    $scope.removePolicy = function(policyKey, insuredName, zip) {
        user.removePolicy($scope.insuredData.user.id, policyKey, insuredName, zip)
            .then(function(associatedPolicies) {
                $scope.refreshPolicies();
            })
            .catch(function(err) {
                $scope.alert("There was a problem removing the policy.", true);
            })
    };
    $scope.$watch("insuredData.user.id", function() {
        $scope.refreshPolicies();
    });
}]);