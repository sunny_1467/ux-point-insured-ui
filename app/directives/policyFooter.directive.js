app.directive('policyFooter', ["$location", function($location) {

    return {
        restrict: "E",
        templateUrl: "app/templates/policyFooter.template.html",
        link: function(scope, element, attrs) {
            $('[data-toggle="tooltip"]').tooltip()
        }
    };
}]);