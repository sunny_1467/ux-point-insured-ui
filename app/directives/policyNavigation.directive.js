app.directive('policyNavigation', ["$location", function($location) {

    return {
        restrict: "E",
        template: "<ul class=\"nav nav-pills nav-justified\">" +
                "<li role=\"presentation\" data-screen=\"/policySummary\"><a ng-click=\"navigate('policySummary')\">Summary</a></li>" +
                "<li role=\"presentation\" data-screen=\"/policyBilling\"><a ng-click=\"navigate('policyBilling')\">Billing</a></li>" +
                "<li role=\"presentation\" data-screen=\"/policyDocuments\"><a ng-click=\"navigate('policyDocuments')\">Documents</a></li>" +
                "<li role=\"presentation\" data-screen=\"/policyPaperless\"><a id=\"paperlessLink\" ng-click=\"navigate('policyPaperless')\"></a></li>" +
            "</ul>",
        link: function(scope, element, attrs) {
            $("li[data-screen='" + $location.path() + "']").addClass("active");
            if (attrs.isPaperless === "true") {
                $("#paperlessLink").text("Go Green")
            } else {
                $("#paperlessLink").text("Go Paper")
            }
        }
    };
}]);