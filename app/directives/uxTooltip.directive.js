app.directive('uxTooltip', [function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            $(element).tooltip({
              placement: "top"
            })
        }
    };
}]);