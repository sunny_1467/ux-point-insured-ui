app.controller("findPolicy", ["$scope", "$location", "policy", "user", function ($scope, $location, policy, user) {
    function pad(pad, str, padLeft) {
        if (typeof str === 'undefined')
            return pad;
        if (padLeft) {
            return (pad + str).slice(-pad.length);
        } else {
            return (str + pad).substring(0, pad.length);
        }
    }

    $scope.findPolicy = function () {
        var policyKey = pad("   ", $scope.KEYSYMBOL) + pad("0000000", $scope.KEYNUMBER, true) + pad("00", $scope.KEYMODULE, true);
        //Based on whether user is signed in, either associate the policy or simply populate the list with it
        if ($scope.insuredData.user.id) {
            user.associatePolicy($scope.insuredData.user.id, policyKey, $scope.INSUREDNAME, $scope.ZIP)
                .then(function (response) {
                    if (response.Status === "Success") {
                        $scope.refreshPolicies()
                            .then(function() {
                                $scope.navigate("associatedPolicies");
                            })
                    } else {
                        $scope.alert(response.Message, true);
                    }
                })
        } else {
            policy.searchPolicy(policyKey, $scope.INSUREDNAME, $scope.ZIP)
                .then(function (policy) {
                    $scope.insuredData.policies.push(policy);
                    $scope.navigate("associatedPolicies");
                });

        }
    }
}]);