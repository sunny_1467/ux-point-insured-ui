app.controller("logins", ["$scope", "$location", "uxConfig", function($scope, $location, uxConfig) {
    $scope.clientIds = uxConfig.clientIds;
    $location.search("policyKey", null);
    $scope.login = function(network) {
        hello(network).login();
    }
    $scope.findPolicy = function() {
        $location.path('/findPolicy');
    }
}]);