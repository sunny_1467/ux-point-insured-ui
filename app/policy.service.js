app.factory("policy", ["$q", "$http", function ($q, $http) {
    /** Mocks an HTML call by resolving the passed object a short time later. */
    function httpMock(returnObject) {
        var mock = $q.defer();
        setTimeout(function () { mock.resolve(returnObject); }, 500);
        return mock.promise;
    }
    function policyMock() {
        return {
            policyKey: "0005HP 400054700",
            lineOfBusiness: "Homeowners",
            keySymbol: "HP ",
            keyNumber: "4000547",
            keyModule: "00",
            insuredName: "JIMMY NOVAK",
            addressLine1: "100 Shady Lane",
            addressLine2: "",
            city: "Stuckey",
            state: "SC",
            zip: "29223",
            email: "someone@somewhere.com",
            paperless: false,
            effectiveDate: 1479099600000,
            expirationDate: 1510635600000,
            status: "Active",
            agentName: "AFFINITY INSURANCE SERVICE INC",
            agentAddress: "159 E COUNTY LINE RD, 19040",
            agentEmail: "(804)524-2535",
            agentPhone: "AFFINITYAGT@EMAIL.COM"
        };
    }
    var policy = {
        /** Takes a set of data entered by the user and searches for the policy on the PIJ DB. */
        searchPolicy: function (policyKey, policyInsured, policyZip) {
            var url = "/ux-point-insured-api/InsData/InsPolicyDataValidation/" + policyKey + "/" + policyZip + "/" + policyInsured;
            console.debug("searchPolicy", url);
            return httpMock({ "Status": "Success", "data": { "data": [{ "EXPIRATIONDATE": "06/18/2019", "EFFECTIVEDATE": "06/18/2018", "KEYSYMBOL": "HP", "STATE": "SC", "ADDRESSLINE1": "413 Aiken Street", "ADDRESSLINE2": "", "AGENTPOSTAL": "26223", "KEYMODULE": "00", "AGENTEMAIL": "", "STATUS": "Future", "AGENTSTATE": "SC", "AGENTNAME": "XCHANGE GROUP TEST", "POLICYKEY": "0005HP400251100", "AGENTCITY": "COLUMBIA", "KEYNUMBER": "4002511", "ZIP": "29801", "AGENCYNO": "0000001", "AGENTADDRESS": "MAIN STREET", "LINEOFBUSINESS": "HP", "AGENTPHONE": "0", "PAPERLESS": "E", "EMAIL": "psharma2080@csc", "CITY": "Aiken", "INSUREDNAME": "LISA Lander" }] } })
                .then(function (response) { 
                    if (response.data.data.length === 0) {
                        throw "No policies were found matching the criteria."
                    } else {
                        return response.data.data[0]; 
                    }
                });
        },
        /** Sets the paperless flag on the policy, optionally with an updated email address. */
        setPaperless: function (policyKey, paperless, newEmail) {
            var paperCode = (paperless) ? "E" : "P";
            var url = "/ux-point-insured-api/InsData/setPaperless/" + policyKey + "/" + paperCode + "/" + newEmail;
            console.debug("setPaperless", url);
            return httpMock({ "Status": "Success" });
        },
        /** Returns billing data by policy key. */
        getBilling: function (policyKey, policyInsured, policyZip) {
            var url = "/ux-point-insured-api/InsData/getBillingInfo/" + policyKey + "/" + policyZip + "/" + policyInsured;
            console.debug("getBilling", url);
            return httpMock({ "data": [{ "BALANCE": "543.50", "TOTALTODATE": "0", "PREMIUM": "595.00", "MINIMUMDUE": "0.00", "DUEBY": "1190301", "PLANNAME": "Agency Bill A3-Quarterly 33%", "PAIDON": "0", "POLICYKEY": "0005FP100068700", "LASTPAYMENT": "0" }] })
                .then(function(response) {
                    if (response.data.length === 0) {
                        throw "No policies were found matching the criteria."
                    } else {
                        return response.data[0]; 
                    }               
                });
        },
        /** Returns a list of media management policy documents given a policy key. */
        getDocuments: function (userId, policyKey, policyInsured, policyZip) {
            var url = "/ux-point-insured-api/InsData/getDocumentList/" + userId + "/" + policyKey + "/" + policyZip + "/" + policyInsured;
            console.debug("getDocuments", url);
            return httpMock({ "Status": "Success", "data": [{ "summary": { "MMAgent": "0000358", "MMCompany": "CSC", "MMDatePolEff": "10/20/2016", "MMRecipList": "Company Copy:COM,Insured Copy:INS,Agent Copy:AGT", "MMLob": "Homeowners", "MMDateProd": "2016-10-07", "MMDocNum": "HP 400271200", "MMAgency": "0000358", "MMDateRcvBill": "10/7/2016", "MMName": "Romi Gupta", "MMTrnType": "EN", "MMDescription": "Change as of", "MMMultiDoc": "Y", "MMDateViewed": "before 1/1/2017", "key": "K:00588F806300000002,D:20161020" } }] })
                .then(function (response) { return response.data; });
        },
        /** Returns the API URL for a document PDF link. */
        getDocumentLink: function(userId, document) {
            var url = "/ux-point-insured-api/InsData/getDocument/" + userId + "/" + document.key;
            return url;
        }
    }
    return policy;
}]);