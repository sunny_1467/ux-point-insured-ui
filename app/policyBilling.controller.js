app.controller("policyBilling", ["$scope", "$location", "uxConfig", "policy", function($scope, $location, uxConfig, policy) {
    $scope.policyCurrent = $scope.insuredData.policies[($location.search().policyIndex) ? $location.search().policyIndex : 0];

    policy.getBilling($scope.policyCurrent.policyKey, $scope.policyCurrent.INSUREDNAME, $scope.policyCurrent.ZIP)
        .then(function(data) {
            $scope.billingData = data
        });
}]);