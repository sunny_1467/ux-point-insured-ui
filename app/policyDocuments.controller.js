app.controller("policyDocuments", ["$scope", "$location", "uxConfig", "policy", function($scope, $location, uxConfig, policy) {
    $scope.policyCurrent = $scope.insuredData.policies[($location.search().policyIndex) ? $location.search().policyIndex : 0];
    var userId = ($scope.insuredData.user.id) ? $scope.insuredData.user.id : "anonymous"
    policy.getDocuments(userId, $scope.policyCurrent.policyKey, $scope.policyCurrent.INSUREDNAME, $scope.policyCurrent.ZIP)
        .then(function(data) {
            $scope.documents = data
        });

    $scope.openDocument = function(document) {
        window.open(policy.getDocumentLink(userId, document));
    }
}]);