app.controller("policyPaperless", ["$scope", "$location", "uxConfig", "policy", "user", function($scope, $location, uxConfig, policy, user) {
    $scope.policyCurrent = $scope.insuredData.policies[($location.search().policyIndex) ? $location.search().policyIndex : 0];

    $scope.togglePaperless = function() {
        policy.setPaperless($scope.policyCurrent.policyKey, !$scope.isPaperless($scope.policyCurrent), $scope.newEmail)
            .then(function() {
                $location.path("/policySummary");
            })
            .catch(function(err) {
                $scope.alert("There was a problem setting the paperless option.", true);
            })
    };
}]);