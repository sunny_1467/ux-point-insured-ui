app.factory("user", ["$q", "$http", function ($q, $http) {
    /** Mocks an HTML call by resolving the passed object a short time later. */
    function httpMock(returnObject) {
        var mock = $q.defer();
        setTimeout(function () { mock.resolve(returnObject); }, 500);
        return mock.promise;
    }
    function policyMock() {
        return {
            policyKey: "0005HP 400054700",
            lineOfBusiness: "Homeowners",
            keySymbol: "HP ",
            keyNumber: "4000547",
            keyModule: "00",
            insuredName: "JIMMY NOVAK",
            addressLine1: "100 Shady Lane",
            addressLine2: "",
            city: "Stuckey",
            state: "SC",
            zip: "29223",
            email: "someone@somewhere.com",
            paperless: false,
            effectiveDate: 1479099600000,
            expirationDate: 1510635600000,
            status: "Active",
            agentName: "AFFINITY INSURANCE SERVICE INC",
            agentAddress: "159 E COUNTY LINE RD, 19040",
            agentEmail: "(804)524-2535",
            agentPhone: "AFFINITYAGT@EMAIL.COM"
        };
    }
    var user = {
        /** Returns an array of full policy data for each policy associated with the passed external id. */
        getUser: function (userId) {
            var url = "/ux-point-insured-api/InsData/getUser/" + userId;
            console.debug("getUser", url);
            if (!userId) {
                return httpMock(null);
            } else {
                return httpMock({ "data": [
                    { "EXPIRATIONDATE": "02/16/2018", "EFFECTIVEDATE": "02/16/2017", "KEYSYMBOL": "HP", "STATE": "SC", "ADDRESSLINE1": "", "ADDRESSLINE2": "ONE MIRACLE WAY", "AGENTPOSTAL": "11111-1111", "KEYMODULE": "00", "AGENTEMAIL": "Work_email_address@csc.com", "STATUS": "Future", "AGENTSTATE": "NE", "AGENTNAME": "Wachovia Bank", "POLICYKEY": "0005HP400127900", "AGENTCITY": "DELHI", "KEYNUMBER": "4001279", "ZIP": "29201-1111", "AGENCYNO": "0000358", "AGENTADDRESS": "Noida", "LINEOFBUSINESS": "HP", "AGENTPHONE": "4555666777 00000", "PAPERLESS": "E", "EMAIL": "fasdfsfa208@csc", "CITY": "COLUMBIA", "INSUREDNAME": "A A A" }, 
                    { "EXPIRATIONDATE": "07/31/2018", "EFFECTIVEDATE": "01/31/2018", "KEYSYMBOL": "HP", "STATE": "TX", "ADDRESSLINE1": "7380 Dominica", "ADDRESSLINE2": "", "AGENTPOSTAL": "75023", "KEYMODULE": "00", "AGENTEMAIL": "", "STATUS": "Future", "AGENTSTATE": "TX", "AGENTNAME": "Mr John Ted", "POLICYKEY": "0042HP400127900", "AGENTCITY": "Plano", "KEYNUMBER": "4001279", "ZIP": "29201-1111", "AGENCYNO": "0000001", "AGENTADDRESS": "1 Main Street", "LINEOFBUSINESS": "HP", "AGENTPHONE": "", "PAPERLESS": "E", "EMAIL": "", "CITY": "Bronsvilee", "INSUREDNAME": "Hunter Dinkins" }, 
                    { "EXPIRATIONDATE": "06/18/2019", "EFFECTIVEDATE": "06/18/2018", "KEYSYMBOL": "HP", "STATE": "FL", "ADDRESSLINE1": "NOIDA", "ADDRESSLINE2": "", "AGENTPOSTAL": "91900-0000", "KEYMODULE": "00", "AGENTEMAIL": "", "STATUS": "Future", "AGENTSTATE": "IN", "AGENTNAME": "Mr. Lucy Dinkins", "POLICYKEY": "0009HP400251200", "AGENTCITY": "Gary", "KEYNUMBER": "4002512", "ZIP": "11002-2", "AGENCYNO": "2345678", "AGENTADDRESS": "2254 Red Hawk", "LINEOFBUSINESS": "HP", "AGENTPHONE": "", "PAPERLESS": "P", "EMAIL": "", "CITY": "Noida", "INSUREDNAME": "MONDAY BLUES" }] })
                    .then(function (response) { return response.data; });
            }
        },
        /** Associates a new policy key with an external id. */
        associatePolicy: function (userId, policyKey, policyInsured, policyZip) {
            var url = "/ux-point-insured-api/InsData/InsPolicyDataInsert/" + userId + "/" + policyKey + "/" + policyZip + "/" + policyInsured;
            console.debug("associatePolicy", url);
            return httpMock({ "Status": "Success" })
        },
        /** Removes an existing policy key that is associated with an external id. */
        removePolicy: function (userId, policyKey, policyInsured, policyZip) {
            var url = "/ux-point-insured-api/InsData/InsDelData/" + userId + "/" + policyKey + "/" + policyZip + "/" + policyInsured;
            console.debug("removePolicy", url);
            return httpMock({ "Status": "Success", "Message": "Policy removed from the user successfully" })
        }
    }
    return user;
}]);